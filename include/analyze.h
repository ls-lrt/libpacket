#ifndef ANALYZE_H
#define ANALYZE_H

void dump_packet( uint8_t * buffer, int len );
void analyze_packet( uint8_t * buffer, int len );

#endif // ANALYZE_H
